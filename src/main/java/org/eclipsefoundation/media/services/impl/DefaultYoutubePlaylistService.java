/*********************************************************************
* Copyright (c) 2022, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.services.impl;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipsefoundation.core.helper.LoggingHelper;
import org.eclipsefoundation.core.model.ParameterizedCacheKey;
import org.eclipsefoundation.core.service.LoadingCacheManager;
import org.eclipsefoundation.media.config.YoutubeMediaProviderConfig;
import org.eclipsefoundation.media.models.YoutubePlaylist;
import org.eclipsefoundation.media.services.YoutubePlaylistService;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.runtime.Startup;

/**
 * Default implementation of the YoutubePlaylistService. Uses a loading cache for all playlists by channel.
 */
@Startup
@ApplicationScoped
public class DefaultYoutubePlaylistService implements YoutubePlaylistService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultYoutubePlaylistService.class);

    @Inject
    YoutubeMediaProviderConfig config;

    @Inject
    LoadingCacheManager cacheManager;

    @PostConstruct
    public void init() {
        // Pre-load cache with playlist list for each channel
        config
                .channels()
                .values()
                .parallelStream()
                .forEach(s -> cacheManager
                        .getList(ParameterizedCacheKey
                                .builder()
                                .setId(s)
                                .setClazz(YoutubePlaylist.class)
                                .setParams(new MultivaluedMapImpl<>())
                                .build()));
    }

    @Override
    public List<YoutubePlaylist> getPlaylistsByChannel(String channelId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching playlists for channel ID: {}", LoggingHelper.format(channelId));
        }
        return cacheManager
                .getList(ParameterizedCacheKey
                        .builder()
                        .setId(channelId)
                        .setClazz(YoutubePlaylist.class)
                        .setParams(new MultivaluedMapImpl<>())
                        .build());
    }

    @Override
    public Optional<YoutubePlaylist> getPlaylistById(String playlistId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching Playlist with ID: {}", LoggingHelper.format(playlistId));
        }

        // For each channel, attempt to find matching playlist.
        for (String channelId : config.channels().values()) {
            Optional<YoutubePlaylist> result = getPlaylistsByChannel(channelId)
                    .stream()
                    .filter(p -> p.getId().equalsIgnoreCase(playlistId))
                    .findFirst();
            // if we found a result, return it
            if (result.isPresent()) {
                return result;
            }
        }
        return Optional.empty();
    }
}
