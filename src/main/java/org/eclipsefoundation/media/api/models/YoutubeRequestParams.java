/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.api.models;

import javax.annotation.Nullable;
import javax.ws.rs.QueryParam;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Entity used as a BeanParam for requests to the Youtube API. Contains all
 * potential request parameters used by this application.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_YoutubeRequestParams.Builder.class)
public abstract class YoutubeRequestParams {

    @QueryParam("key")
    public abstract String getKey();

    @Nullable
    @QueryParam("id")
    public abstract String getId();

    @Nullable
    @QueryParam("channelId")
    public abstract String getChannelId();

    @Nullable
    @QueryParam("playlistId")
    public abstract String getPlaylistId();

    @Nullable
    @QueryParam("pageToken")
    public abstract String getPageToken();

    @Nullable
    @QueryParam("maxResults")
    public abstract Integer getMaxResults();

    @QueryParam("part")
    public abstract String getPart();

    public static Builder builder() {
        return new AutoValue_YoutubeRequestParams.Builder()
                .setPart("id,snippet,player");
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setKey(String key);

        public abstract Builder setId(@Nullable String id);

        public abstract Builder setChannelId(@Nullable String id);

        public abstract Builder setPlaylistId(@Nullable String id);

        public abstract Builder setPageToken(@Nullable String token);

        public abstract Builder setMaxResults(@Nullable Integer max);

        public abstract Builder setPart(String part);

        public abstract YoutubeRequestParams build();
    }
}