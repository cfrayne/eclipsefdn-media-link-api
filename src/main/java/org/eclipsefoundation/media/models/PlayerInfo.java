/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * An object containing the embeddeable HTML of the playlist/video.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_PlayerInfo.Builder.class)
public abstract class PlayerInfo {

    public abstract String getEmbedHtml();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_PlayerInfo.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        @JsonProperty("embedHtml")
        public abstract Builder setEmbedHtml(String embedHtml);

        public abstract PlayerInfo build();
    }
}
