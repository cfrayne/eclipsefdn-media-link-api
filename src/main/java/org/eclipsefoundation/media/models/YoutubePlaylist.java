/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.models;

import java.time.ZonedDateTime;

import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Response object for requested playlist
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_YoutubePlaylist.Builder.class)
public abstract class YoutubePlaylist {

    public abstract String getId();

    public abstract String getEtag();

    public abstract PlaylistSnippet getSnippet();

    public abstract PlayerInfo getPlayer();

    public abstract Builder toBuilder();

    public static Builder builder() {
        return new AutoValue_YoutubePlaylist.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setId(String id);

        public abstract Builder setEtag(String etag);

        public abstract Builder setSnippet(PlaylistSnippet snippet);

        public abstract Builder setPlayer(PlayerInfo player);

        public abstract YoutubePlaylist build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_YoutubePlaylist_PlaylistSnippet.Builder.class)
    public abstract static class PlaylistSnippet {
       
        public abstract ZonedDateTime getPublishedAt();

        public abstract String getChannelId();

        public abstract String getTitle();

        @Nullable
        @JsonProperty("description")
        public abstract String getDescription();

        public abstract ThumbnailOptions getThumbnails();

        public abstract String getChannelTitle();

        public abstract LocalizedInfo getLocalized();

        public static Builder builder() {
            return new AutoValue_YoutubePlaylist_PlaylistSnippet.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            @JsonProperty("publishedAt")
            public abstract Builder setPublishedAt(ZonedDateTime date);

            @JsonProperty("channelId")
            public abstract Builder setChannelId(String id);

            public abstract Builder setTitle(String title);

            @JsonProperty("description")
            public abstract Builder setDescription(@Nullable String desc);

            public abstract Builder setThumbnails(ThumbnailOptions thumbnails);

            @JsonProperty("channelTitle")
            public abstract Builder setChannelTitle(String title);

            public abstract Builder setLocalized(LocalizedInfo localized);

            public abstract PlaylistSnippet build();
        }
    }
}
